# Maxime Tournier
- ❤~ 17 years old, France
- 🧡~ Founder Director of [Tyrolium](https://tyrolium.fr/)
- 💛~ Web Developer training at [HumanBooster](https://humanbooster.com/)
- 💚~ The Languages that I know - `Java`, `PHP`, `JS`, `TS`, `HTML`, `CSS`, `SQL`
- 💜~ The FrameWork/Lib/Engine that I know - [`Gradle`](https://gradle.org/), [`ForgeMC`](https://files.minecraftforge.net/net/minecraftforge/forge/), [`SpigotMC`](https://www.spigotmc.org/), [`Angular`](https://angular.io/), [`NodeJS`](https://nodejs.org/), [`Electron`](https://www.electronjs.org/), [`Boostrap`](https://getbootstrap.com/), [`Symfony`](https://symfony.com/), [`MySQL`](https://www.mysql.com/), [`MariaDB`](https://mariadb.org/) et [`PhpMyAdmin`](https://www.phpmyadmin.net/)
- 💙~ My Software and OS : [`GNU/Linux - Ubuntu Desktop`](https://ubuntu.com/) & [`GNU/Linux - Kubuntu Desktop`](https://kubuntu.org/), [`Windows 10`](https://www.microsoft.com/fr-fr/windows/) for Gaming and [`GNU/Linux - Debian Server`](https://www.debian.org/) for my Server  - Ide : [`Intellij Idea`](https://www.jetbrains.com/fr-fr/idea/) and [`Visual Studio Code`](https://code.visualstudio.com/)
- 🖤~ Developer/Streamer - My [Twitch](twitch.tv/themaximesan)
- 🤎~ Join me on my [Discord](https://discord.gg/mtDx9ceS7n)

## My Organizations on Github 
- 🔵 [Organization of Tyrolium](https://github.com/Tyrolium)
- ⚫ [Organization of TyroServ (Minecraft Modifying Community Server Project)](https://github.com/TyroliumServerMC)
- 🔴 [Organization of My Projet Codeless](https://github.com/TheMaximeSan-NoCode)

## My Social Networks
- 🌐~ My E-Mail : maxime.tournier@tyrolium.fr
- 🌐~ My Discord : TheMaximeSan#0001
- 🌐~ My Instagram : [the_maxime_san](https://www.instagram.com/the_maxime_san/)
- 🌐~ My Twitter : [@MaximeTournier2](https://twitter.com/MaximeTournier2)
- 🌐~ My Linkedin : [maxime_tournier_tyrolium](https://www.linkedin.com/in/maxime-tournier-tyrolium/)
```js
Console.log("Hello Tyrolium ;)");
```
